from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.contrib.auth import get_user_model
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout

# Create your views here.
def signup(request):
    if request.method=="POST":
        account_form = UserCreationForm(request.POST)
        if account_form.is_valid():
            user = account_form.save()
            login(request, user)
            return redirect('posts:index')
        
    else: 
        account_form = UserCreationForm()
    context = {'account_form': account_form}
    return render(request,'accounts/form.html', context)

def signout(request):
    logout(request)
    return redirect('posts:index')

def signin(request):
        if request.user.is_authenticated:
            return redirect('posts:index')
        if request.method == "POST": 
            account_form =  AuthenticationForm(request, request.POST)
            if account_form.is_valid():
                login(request, account_form.get_user())
                return redirect('posts:index')
        else:
            account_form = AuthenticationForm()
        context = {'account_form': account_form}
        return render(request,'accounts/form.html', context)

@login_required
def index(request, username):
    User = get_user_model()
    user = get_object_or_404(User, username=username)
    posts = user.post_set.order_by('-pk')
    context = {'user': user, 'posts': posts}
    return render(request, 'posts/index.html', context)