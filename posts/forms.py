from django import forms
from .models import Post, Image

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['content']

class ImageForm(forms.ModelForm):

    class Meta:
        model = Image
        fields = ['image']
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update({'multiple': ''})
        
    def save(self, *args, **kwargs):
        # print(self.fields.get('image'))
    
        # file_list = self.fields.get('image')
        # print(dir(file_list))
        # for file in file_list.values():
        # Image.objects.create(image=file, post=kwargs.get('post'))
        return super().save(*args, **kwargs)