from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

from .models import Post, Image
from .forms import PostForm, ImageForm
# Create your views here.
def index(request):
    posts = Post.objects.order_by('-pk')
    context = {'posts': posts}
    return render(request, 'posts/index.html', context)

def detail(request, post_pk):
    post = get_object_or_404(Post.objects.prefetch_related('image_set'), pk=post_pk)
    images = post.image_set.all()
    context = {'post': post, 'images': images}
    return render(request, 'posts/detail.html', context)

@login_required
def create(request):
    if request.method == 'POST':
        # print(request.FILES)
        # print(request.FILES.get)
        post_form = PostForm(request.POST)
        image_form = ImageForm(request.FILES)
        print(request.FILES)
        if post_form.is_valid():
            post = post_form.save(commit=False)
            post.user = request.user
            post.save()
            for image in request.FILES.getlist('image'):            
                Image.objects.create(image=image, post=post)
            # post.image_set.add()
            return redirect(post)
    else:
        post_form = PostForm()
        image_form = ImageForm()
    context = {'post_form': post_form, 'image_form': image_form}
    return render(request, 'posts/form.html', context)

# def comment_create(request, post_pk):
#     post = get_object_or_404(Post.objects.prefetch_related('image_set'), pk=post_pk)
#     if request.method == 'POST':
#         # print(request.FILES)
#         # print(request.FILES.get)
#         comment_form = CommentForm(request.POST)
#         if comment_form .is_valid():
#             comment = comment_form.save(commit=False)
#             comment.post = post
#             comment.save()
#             return redirect(post)
#     else:
#         comment_form = CommentForm()
#     return redirect(post)