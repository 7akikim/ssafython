from django.urls import path
from . import views

app_name = 'posts'

urlpatterns = [
    path('explore/', views.index, name='index'),
    path('posts/<int:post_pk>/', views.detail, name='detail'),
    path('posts/new/', views.create, name='create'),
    # path('posts/<int:post_pk>/comments', views.comment_create, name='comment_create'),
]
